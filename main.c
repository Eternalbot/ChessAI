#include <stdio.h>
#include <stdlib.h>
#include "server/server.h"
#include "game/game.h"

int main()
{
#ifndef _WIN32
    curl_global_init(CURL_GLOBAL_DEFAULT);
#endif
    Game game;
    Color color = WHITE;
    int endGame = 0;
    printf("ID de la partie : ");
    scanf("%d",&(game.id));
    printf("Couleur de l'IA (0 white, 1 black) : ");
    scanf("%u",&color);
    while(!endGame)
    {
        //Game update
        initGame(&game, color);
        //Waiting for playing
        if(parseChessBoard(&game)==game.AIColor)
        {
            drawBoard(&game.board);
            endGame = play(&game, 0);
            sendMovement(&game);
            printf("Nombre de positions testees : %llu\n", nbrBranches);
            nbrBranches = 0;
        }
        sleep(SECOND);
    }
    return EXIT_SUCCESS;
}
