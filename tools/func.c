#include "../game/turn.h"

void freeAll(Turn* t)
{
    if(t != NULL)
    {
        //Recursive memory release
        freeAll(t->son);
        free(t);
        t = NULL;
    }
}
