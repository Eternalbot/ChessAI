#ifndef GAME_H
#define GAME_H
#include <stdlib.h>
#include <stdio.h>
#ifdef _WIN32
#include <windows.h>
#endif
#include "turn.h"
#ifdef _WIN32
#define sleep Sleep
#define SECOND 1000
#else
#include <unistd.h>
#define SECOND 1
#endif

/**
 * \file game\game.h"
 * \brief Structures and functions required to compute the basis of a game
 * \author Mathieu L.
 * \version 3
 */

/**
 * \struct game "game\game.h"
 * \brief Struct of the game
 */
/**
 * \typedef Game
 * \brief Struct of the game
 */
typedef struct game
{
    ChessBoard board;   /*!< The current board of the game*/
    Color AIColor;      /*!< The color of the AI*/
    int id;             /*!< The ID of the game on the server*/
    Movement movement;  /*!< The move to perform*/
    int time;           /*!< time by turn*/
}Game;

/**
 * \fn void initGame(Game* game, const Color AI)
 * \brief Initiate the game
 * \param *game
 *          Game pointer
 * \param AI
 *          Color of the AI
 */
void initGame(Game* game, const Color AI);

/**
 * \fn void play(Game* game, const int noeuds)
 * \brief Start the recursive research of the best move
 * \param *game
 *          Game pointer
 * \param n
 *          Wanted depth of the tree
 * \return 1 or -1 if the game is over, 0 otherwise
 */
int play(Game* game, const int n);

#endif // GAME_H
