#ifndef TURN_H
#define TURN_H
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <pthread.h>
#include "chessboard.h"
#include "tools/heuristic.h"

/**
 * \file game\turn.h"
 * \brief Structures and functions required to compute the turns of a game
 * \author Mathieu L.
 * \version 3
 */

/**
 * \var nbrBranches
 * \brief the number of positions the AI computes
 */
static Bitboard nbrBranches;

/**
 * \struct turn
 * \brief Struct of the turn of a game
 */
/**
 * \typedef Turn
 * \brief Struct of the turn of a game
 */
typedef struct turn
{
    ChessBoard board;                /*!< The current board of the game*/
    Color player;                    /*!< The player of the turn*/
    Color AIColor;                   /*!< The color of the AI*/
    int (*evaluation)(ChessBoard*);  /*!< The evaluation function */
    struct turn* son;                /*!< The following board of the game*/
    int heuristic;                   /*!< The current heuristic of the turn, based on the son's*/
    Movement movement;               /*!< The best move to perform*/
    int alpha;                       /*!< Alpha member of the alphabeta algorithm*/
    int beta;                        /*!< Beta member of the alphabeta algorithm*/
    int nodeDepth;                   /*!< Depth of the node*/
    int maximalDepth;                /*!< Maximal depth*/
    int* time;                       /*!< Time left to find a movement*/
}Turn;

/**
 * \fn void createPossibilies(Turn* turn)
 * \brief Create the tree of possibilities for the given depth
 * \param *turn
 *          Turn pointer
 * \return 1 or -1 if the game is over, 0 otherwise
 */
int createPossibilies(Turn* turn);

/**
 * \fn void showBestGame(Turn* turn)
 * \brief Show the detail of the best moves throw the tree of possibilities
 * \param *turn
 *          Turn pointer
 */
void showBestGame(Turn* turn);

/**
 * \fn int check(Turn* turn)
 * \brief Verify if the board is in a check state
 * \param *turn
 *          Turn pointer
 * \return 0 if there is no check situation and 1 if the player is in a check situation.
 *  if the player cannot move without lose his king :
 *  0 -> drawn
 *  1 -> checkmate
 */
int check(Turn* turn);

/**
 * \fn void freeAll(Turn* t)
 * \brief Free all the pointers stored in the structure
 * \param *t
 *          Turn pointer
 */
void freeAll(Turn* t);
#endif // TURN_H
