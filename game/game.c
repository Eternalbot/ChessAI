#include "game.h"

void initGame(Game* game, const Color AI)
{
    initChessBoard(&(game->board));
    game->AIColor = AI;
}
void startTimer(int* timer)
{
    //The timer decrease the remaining time each second.
    while(*timer>5)
    {
        (*timer)--;
        sleep(SECOND);
    }
}

int play(Game* game, const int n)
{
    game->movement = 0;
    int endGame = 0;
    char end = 0;
    //Create the tree and choose the best moves
    int i = 1;
    int timer = game->time;
    //Start the timer
    pthread_t iterative;
    pthread_create(&iterative, NULL, startTimer, &timer);
    //Continue to search the best solution until time is up
    while(timer>(game->time/2) && !end)
    {
        Turn* turn;
        turn = malloc(sizeof(Turn));
        turn->player = game->AIColor;
        turn->movement = 0;
        turn->board = game->board;
        turn->son = NULL;
        turn->AIColor = game->AIColor;
        turn->evaluation = (game->AIColor == WHITE ? whiteEval : blackEval);
        turn->alpha = -200000;
        turn->beta = 200000;
        turn->time = &timer;
        turn->nodeDepth = i;
        turn->maximalDepth = i;
        turn->heuristic = 0;
        printf("Recherche avec profondeur %d\n", i);
        //Search
        createPossibilies(turn);
        //If there is still time after a research, update of the best move
        if(timer>5)
        {
            if(turn->heuristic > KING_VALUE/2 || turn->heuristic < -KING_VALUE/2)
                end = 1; //checkmate found, end of the game
            game->movement = turn->son->movement;
            printf("Deplacement de %d a %d\n", (game->movement>>START_POS)&BITS_POS, (game->movement>>END_POS)&BITS_POS);
            printf("Heuristique = %d\n", turn->son->heuristic);
            //freeAll(turn->son);
        }
        free(turn);
        printf("---\n");
        i++;
    }
    timer = 0;
    //Stopping the timer
    pthread_join(iterative, NULL);
    printf("Recherche terminee\n");
    printf("Deplacement choisit : de %d a %d\n", (game->movement>>START_POS)&BITS_POS, (game->movement>>END_POS)&BITS_POS);
    return endGame;
}

