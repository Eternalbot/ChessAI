#ifndef HEURISTIC_H
#define HEURISTIC_H
#include "../chessboard.h"

/**
 * \file "game\tools\heuristic.h"
 * \brief Structures and functions required to compute the heuristic of a chessboard
 * \author Mathieu L.
 * \version 1
 */

/**
 * \fn int whiteEval(ChessBoard* board)
 * \brief Compute the action member of the heuristic of the given chessboard (white version)
 * \param *board
 *          Chessboard pointer
 * \return The action member of the heuristic.
 */
int whiteEval(ChessBoard* board);

/**
 * \fn int blackEval(ChessBoard* board)
 * \brief Compute the action member of the heuristic of the given chessboard (black version)
 * \param *board
 *          Chessboard pointer
 * \return The action member of the heuristic.
 */
int blackEval(ChessBoard* board);

#endif // HEURISTIC_H
