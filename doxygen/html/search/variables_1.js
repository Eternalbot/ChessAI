var searchData=
[
  ['beta',['beta',['../structturn.html#a684b1df447558ad94b70028b3292227e',1,'turn']]],
  ['blackbishop',['blackBishop',['../structboard.html#ae52e5a4018379528a89f69968bb7378c',1,'board']]],
  ['blackking',['blackKing',['../structboard.html#a7da85e8008c37529352340b139c79cb0',1,'board']]],
  ['blackknight',['blackKnight',['../structboard.html#ad65b56ff110ba9ed17f5f42f6e5d13cd',1,'board']]],
  ['blackmastering',['blackMastering',['../structboard.html#a79db081f959f05f4729222e3592ba5b2',1,'board']]],
  ['blackpawn',['blackPawn',['../structboard.html#a1fde5d4e0f17d925325c9a11eb07e898',1,'board']]],
  ['blackpieces',['blackPieces',['../structboard.html#a4eb8b5752a73ccd95a9bda4a4153e03c',1,'board']]],
  ['blackrook',['blackRook',['../structboard.html#abf2d67c7407bf5f48bf54dc02a51fa77',1,'board']]],
  ['board',['board',['../structgame.html#a1d143fbb94c384ba999c4b78bd08445d',1,'game::board()'],['../structturn.html#a1d143fbb94c384ba999c4b78bd08445d',1,'turn::board()']]]
];
