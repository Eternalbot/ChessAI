#ifndef CONSTANTS_H
#define CONSTANTS_H

/**
 * \file "constants\constants.h"
 * \brief All the constants which can be used by the AI
 * \author Mathieu L.
 * \version 1
 */

/**
 * \def MAX_MOVE
 * \brief An augmented value of the maximum number of movement in a turn
 */
#define MAX_MOVE 28

/**
 * \def WHITE_PAWN_LINE
 * \brief The start line for white pawns
 */
#define WHITE_PAWN_LINE 6

/**
 * \def BLACK_PAWN_LINE
 * \brief The start line for black pawns
 */
#define BLACK_PAWN_LINE 1

/**
 * \def SHIFT
 * \brief Binary shift for power of two numbers
 * Useful for adding or removing pieces. Allow the creation of 64 bits integers.
 */
#define SHIFT 1LL

/**
 * \def SIZE
 * \brief Width and height of the board.
 */
#define SIZE 8

/**
 * \def START_POS
 * \brief First bit of the start position in a movement variable.
 */
#define START_POS 9LL

/**
 * \def END_POS
 * \brief First bit of the end position in a movement variable.
 */
#define END_POS 3LL

/**
 * \def BITS_POS
 * \brief Maximum value of a position.
 * Useful to determine the start or the end position from a movement variable
 */
#define BITS_POS 0x3F

/**
 * \def PAWN_VALUE
 * \brief The valuation of a pawn
 */
#define PAWN_VALUE 100

/**
 * \def ROOK_VALUE
 * \brief The valuation of a rook
 */
#define ROOK_VALUE 500

/**
 * \def KING_VALUE
 * \brief The valuation of a king
 */
#define KING_VALUE 20000

/**
 * \def KNIGHT_VALUE
 * \brief The valuation of a knight
 */
#define KNIGHT_VALUE 350

/**
 * \def BISHOP_VALUE
 * \brief The valuation of a bishop
 */
#define BISHOP_VALUE 350

/**
 * \def QUEEN_VALUE
 * \brief The valuation of a bishop
 */
#define QUEEN_VALUE 1000
#endif // CONSTANTS_H
