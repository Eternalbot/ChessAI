# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/mathieu/Documents/ChessAI/cJSON/cJSON.c" "/home/mathieu/Documents/ChessAI/cmake-build-debug/CMakeFiles/ChessAI.dir/cJSON/cJSON.c.o"
  "/home/mathieu/Documents/ChessAI/cJSON/cJSON_Utils.c" "/home/mathieu/Documents/ChessAI/cmake-build-debug/CMakeFiles/ChessAI.dir/cJSON/cJSON_Utils.c.o"
  "/home/mathieu/Documents/ChessAI/game/chessboard.c" "/home/mathieu/Documents/ChessAI/cmake-build-debug/CMakeFiles/ChessAI.dir/game/chessboard.c.o"
  "/home/mathieu/Documents/ChessAI/game/game.c" "/home/mathieu/Documents/ChessAI/cmake-build-debug/CMakeFiles/ChessAI.dir/game/game.c.o"
  "/home/mathieu/Documents/ChessAI/game/tools/heuristic.c" "/home/mathieu/Documents/ChessAI/cmake-build-debug/CMakeFiles/ChessAI.dir/game/tools/heuristic.c.o"
  "/home/mathieu/Documents/ChessAI/game/turn.c" "/home/mathieu/Documents/ChessAI/cmake-build-debug/CMakeFiles/ChessAI.dir/game/turn.c.o"
  "/home/mathieu/Documents/ChessAI/main.c" "/home/mathieu/Documents/ChessAI/cmake-build-debug/CMakeFiles/ChessAI.dir/main.c.o"
  "/home/mathieu/Documents/ChessAI/server/piece.c" "/home/mathieu/Documents/ChessAI/cmake-build-debug/CMakeFiles/ChessAI.dir/server/piece.c.o"
  "/home/mathieu/Documents/ChessAI/server/server.c" "/home/mathieu/Documents/ChessAI/cmake-build-debug/CMakeFiles/ChessAI.dir/server/server.c.o"
  "/home/mathieu/Documents/ChessAI/tools/func.c" "/home/mathieu/Documents/ChessAI/cmake-build-debug/CMakeFiles/ChessAI.dir/tools/func.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../cJSON"
  "../constants"
  "../game"
  "../game/tools"
  "../server"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
