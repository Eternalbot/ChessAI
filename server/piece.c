#include "piece.h"

void createBlackPawn(ChessBoard* board, int pos)
{
    //Add the pawn on the chessboard
    board->blackPawn+=SHIFT<<pos;
    //update the black chessboard mastering
    board->blackMastering += PAWN_TABLE[63-pos];
}

void createBlackRook(ChessBoard* board, int pos)
{
    //Add the rook on the chessboard
    board->blackRook+=SHIFT<<pos;
    //update the black chessboard mastering
    board->blackMastering += ROOK_TABLE[63-pos];
}

void createBlackBishop(ChessBoard* board, int pos)
{
    //Add the bishop on the chessboard
    board->blackBishop+=SHIFT<<pos;
    //update the black chessboard mastering
    board->blackMastering += BISHOP_TABLE[63-pos];
}

void createBlackKnight(ChessBoard* board, int pos)
{
    //Add the knight on the chessboard
    board->blackKnight+=SHIFT<<pos;
    //update the black chessboard mastering
    board->blackMastering += KNIGHT_TABLE[63-pos];
}

void createBlackKing(ChessBoard* board, int pos)
{
    //Add the king on the chessboard
    board->blackKing+=SHIFT<<pos;
    //update the black chessboard mastering
    board->blackMastering += KING_TABLE[63-pos];
}

void createBlackQueen(ChessBoard* board, int pos)
{
    //Add the queen on the chessboard
    board->blackRook+=SHIFT<<pos;
    board->blackBishop+=SHIFT<<pos;
    //update the black chessboard mastering
    board->blackMastering += QUEEN_TABLE[63-pos];
}


void createWhitePawn(ChessBoard* board, int pos)
{
    //Add the pawn on the chessboard
    board->whitePawn+=SHIFT<<pos;
    //update the white chessboard mastering
    board->whiteMastering += PAWN_TABLE[pos];
}

void createWhiteRook(ChessBoard* board, int pos)
{
    //Add the rook on the chessboard
    board->whiteRook+=SHIFT<<pos;
    //update the white chessboard mastering
    board->whiteMastering += ROOK_TABLE[pos];
}

void createWhiteBishop(ChessBoard* board, int pos)
{
    //Add the bishop on the chessboard
    board->whiteBishop+=SHIFT<<pos;
    //update the white chessboard mastering
    board->whiteMastering += BISHOP_TABLE[pos];
}

void createWhiteKnight(ChessBoard* board, int pos)
{
    //Add the knight on the chessboard
    board->whiteKnight+=SHIFT<<pos;
    //update the white chessboard mastering
    board->whiteMastering += KNIGHT_TABLE[pos];
}

void createWhiteKing(ChessBoard* board, int pos)
{
    //Add the king on the chessboard
    board->whiteKing+=SHIFT<<pos;
    //update the white chessboard mastering
    board->whiteMastering += KING_TABLE[pos];
}

void createWhiteQueen(ChessBoard* board, int pos)
{
    //Add the queen on the chessboard
    board->whiteRook+=SHIFT<<pos;
    board->whiteBishop+=SHIFT<<pos;
    //update the white chessboard mastering
    board->whiteMastering += QUEEN_TABLE[pos];
}
